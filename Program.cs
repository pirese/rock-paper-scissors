﻿using System;
using System.Collections.Generic;
using RockPaperScissors.Model;

namespace RockPaperScissors
{
    public class Program
    {
        private static int playerScore;

        private static int computerScore;

        private static List<Hand> hands = new List<Hand>();
        private static void createHands()
        {
            Rock rock = new Rock();
            Paper paper = new Paper();
            Scissors scissors = new Scissors();
            Dynamite dynamite = new Dynamite();

            rock.AddPrey(scissors);
            paper.AddPrey(rock);
            scissors.AddPrey(paper);
            dynamite.AddPrey(rock);
            dynamite.AddPrey(paper);
            dynamite.AddPrey(scissors);

            hands.Add(rock);
            hands.Add(paper);
            hands.Add(scissors);
            hands.Add(dynamite);
        }

        private static void evaluateRound(Hand playerHand, Hand computerHand)
        {
            if (playerHand.IsPrey(computerHand))
            {
                if (computerHand.IsPrey(playerHand))
                {
                    Console.WriteLine("Tie!");
                }
                else
                {
                    Console.WriteLine("Player wins!");
                    playerScore++;
                }
            }
            else
            {
                if (computerHand.IsPrey(playerHand))
                {
                    Console.WriteLine("Computer wins!");
                    computerScore++;
                }
                else
                {
                    Console.WriteLine("Tie!");
                }
            }
        }

        static void Main()
        {
            createHands();

            while (true)
            {
                Console.Write("Player " + playerScore.ToString() + ":");
                Console.Write(computerScore.ToString() + " Computer\r\n");
                Console.WriteLine("Choose a hand to throw:");

                List<string> menuOptions = new List<string>();
                for (int index=0; index < hands.Count; index++)
                {
                    Console.WriteLine((index + 1).ToString() + ") " + hands[index].GetType().Name);
                    menuOptions.Add((index + 1).ToString());
                }

                string menuOption = Console.ReadKey().KeyChar.ToString();
                if (menuOptions.Contains(menuOption.ToString()))
                {
                    Hand playerHand = hands[Int32.Parse(menuOption) - 1];
                    Random random = new Random();
                    Hand computerHand = hands[random.Next(hands.Count)];
                    Console.Clear();
                    Console.WriteLine("Player chose " + playerHand.GetType().Name);
                    Console.WriteLine("Computer chose " + computerHand.GetType().Name);
                    evaluateRound(playerHand, computerHand);
                }
                else
                {
                    Console.Clear();
                }
            }
        }
        
    }
}