﻿using System;
using System.Collections.Generic;

namespace RockPaperScissors.Model
{
    /// <summary>
    /// A representation of a hand in the game
    /// </summary>
    public abstract class Hand
    {

        private List<Hand> prey = new List<Hand>();

        /// <summary>
        /// Adds a hand to this hands prey
        /// </summary>
        /// <param name="hand">The hand to add</param>
        public void AddPrey(Hand hand)
        {
            prey.Add(hand);
        }

        /// <summary>
        /// Determines whether a given hand is the prey of this hand
        /// </summary>
        /// <param name="opponentHand">The opponents hand</param>
        /// <returns>True if this hand preys on the opponent hand</returns>
        public bool IsPrey(Hand opponentHand)
        {
            return prey.Contains(opponentHand);
        }

    }
}
